/*
 * LED.c
 *
 *  Created on: Feb 28, 2020
 *      Author: 20904126
 */

#include "stdbool.h"
#include "stm32f4xx_it.h"
#include "main.h"
#include "Global_variables.h"


uint32_t tick;
TIM_HandleTypeDef htim6;
DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;


uint8_t LED_function(uint8_t LED){
	tick = HAL_GetTick();
	if(tick-Time_now < 20000){

		if(LED == 1){
			HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_9, SET); /*Set record LED on*/
			if ((tick - last_tick > 250) || (last_tick == 0)){
				HAL_GPIO_TogglePin(GPIOA,  GPIO_PIN_7); /*Set button 1 LED on*/
				last_tick = tick;
			}
		} else if(LED == 2){
			HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_9, SET); /*Set record LED on*/
			if ((tick - last_tick > 250) || (last_tick == 0)){
				HAL_GPIO_TogglePin(GPIOB,  GPIO_PIN_6); /*Set button 2 LED on*/
				last_tick = tick;
			}
		} else if(LED == 3){
			HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_9, SET); /*Set record LED on*/
			if ((tick - last_tick > 250) || (last_tick == 0)){
				HAL_GPIO_TogglePin(GPIOC,  GPIO_PIN_7); /*Set button 3 LED on*/
				last_tick = tick;
			}
		} else if(LED == 5){
			if ((tick - last_tick > 250) || (last_tick == 0)){
				HAL_GPIO_TogglePin(GPIOA,  GPIO_PIN_7); /*Set button 1 LED on*/
				last_tick = tick;
			}
		} else if(LED == 6){
			if ((tick - last_tick > 250) || (last_tick == 0)){
				HAL_GPIO_TogglePin(GPIOB,  GPIO_PIN_6); /*Set button 2 LED on*/
				last_tick = tick;
			}
		} else if(LED == 7){
			if ((tick - last_tick > 250) || (last_tick == 0)){
				HAL_GPIO_TogglePin(GPIOC,  GPIO_PIN_7); /*Set button 3 LED on*/
				last_tick = tick;
			}
		}
	}  else{
		HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_7, RESET);
		HAL_GPIO_WritePin(GPIOB,  GPIO_PIN_6, RESET);
		HAL_GPIO_WritePin(GPIOC,  GPIO_PIN_7, RESET);
		HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_9, RESET); /*Switch all LED's off*/
		button = 10;
		last_tick = 0;
		/* Stop button*/
		recording = 0;
		Time_now = 0;
		REC = 0;
		REC_1 =0;
		playback = 0;
		stop = 0;
		first = 0;
		HAL_TIM_Base_Stop(&htim6);
		HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
	}
	return 0;
}
