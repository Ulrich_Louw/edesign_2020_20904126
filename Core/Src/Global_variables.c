/*
 * Global_variables.c
 *
 *  Created on: Feb 29, 2020
 *      Author: 20904126
 */

#include <stdbool.h>
#include <stdint.h>

extern volatile uint8_t button = 10;
extern volatile bool recording = 0;
extern volatile uint32_t Time_now = 0;
extern volatile bool REC = 0;
extern volatile bool REC_1 = 0;
extern volatile bool playback = 0;
extern volatile bool stop = 0;
extern volatile bool first = 0;
extern uint32_t last_tick = 0;
extern uint16_t dacbuffer[1024];
