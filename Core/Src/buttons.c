/*
 * buttons.c
 *
 *  Created on: Feb 28, 2020
 *      Author: 20904126
 */
#include "stdbool.h"
#include "stm32f4xx_it.h"
#include "main.h"
#include "Global_variables.h"
#include "sinewave.h"

UART_HandleTypeDef huart2;
uint8_t bounce;
uint8_t bounce_record =0;
TIM_HandleTypeDef htim2;
DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;
uint8_t bounce_stop=0;
uint32_t Time_now_stop=0;
uint32_t Time_now_record=0;
extern volatile uint16_t dacbuffer[1024];

void Stop_reset_function(){

	uint8_t function_4[10] = {127,128,'s','t','o','p','_','_','_','_'};
	HAL_UART_Transmit(&huart2,function_4,10,100);
	HAL_TIM_Base_Stop(&htim2);
	HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
	HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_7, RESET);
	HAL_GPIO_WritePin(GPIOB,  GPIO_PIN_6, RESET);
	HAL_GPIO_WritePin(GPIOC,  GPIO_PIN_7, RESET);
	HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_9, RESET); /*Switch all LED's off*/
	button = 10;
	last_tick = 0;
	/* Stop button*/
	recording = 0;
	Time_now = 0;
	REC = 0;
	playback = 0;
	stop = 0;
	first = 0;
	REC_1 = 0;
	Time_now_stop=0;
	Time_now_record=0;

}

uint8_t Button_function(uint8_t button){

		if (button < 4){

			if (button == 1){
				if (first == 1){

					uint8_t function_1[10] = {127,128,'r','e','c','o','r','d','_','1'}; /*Recording for button 1*/
					HAL_UART_Transmit(&huart2,function_1,10,100);
					first = 0;
				}

			} else if (button == 2){
				if (first == 1){
					uint8_t function_2[10] = {127,128,'r','e','c','o','r','d','_','2'}; /*Recording for button 2*/
					HAL_UART_Transmit(&huart2,function_2,10,100);
					first = 0;
				}

			} else if (button == 3){
				if (first == 1){
					uint8_t function_3[10] = {127,128,'r','e','c','o','r','d','_','3'}; /*Recording for button 3*/
					HAL_UART_Transmit(&huart2,function_3,10,100);
					first = 0;
				}

			}


		} else if (button >= 5){
			if (button == 5){

				if (first == 1){
					uint8_t function_5[10] = {127,128,'p','l','a','y','_','_','_','1'}; /*Playback for button 1*/
					HAL_UART_Transmit(&huart2,function_5,10,100);
					first = 0;
				}

			} else if (button == 6){

				if (first == 1){
					uint8_t function_6[10] = {127,128,'p','l','a','y','_','_','_','2'}; /*Playback for button 2*/
					HAL_UART_Transmit(&huart2,function_6,10,100);
					first = 0;
				}

			} else if (button == 7){

				if (first == 1){

					uint8_t function_7[10] = {127,128,'p','l','a','y','_','_','_','3'}; /*Playback for button 3*/
					HAL_UART_Transmit(&huart2,function_7,10,100);
					first = 0;
				}
			}


		}


	return 0;
}

bool Check_Bounce(void){

	if((HAL_GetTick() - Time_now > 10)&&(Time_now != 0)) {

		bounce = 1;

	} else if(Time_now == 0){
		/* If time is more than 10ms */
		bounce = 0;
		Time_now = HAL_GetTick();

	} else {
		bounce = 0;

	}
	return bounce;
}

bool Check_StopBounce(void){

	if((HAL_GetTick() - Time_now_stop > 10)&&(Time_now_stop != 0)) {

		bounce_stop = 1;

	} else if(Time_now_stop == 0){
		/* If time is more than 10ms */
		bounce_stop = 0;
		Time_now_stop = HAL_GetTick();

	} else {
		bounce_stop = 0;

	}
	return bounce_stop;
}

bool Check_RecordBounce(void){

	if((HAL_GetTick() - Time_now_record > 10)&&(Time_now_record != 0)) {

		bounce_record = 1;

	} else if(Time_now_record == 0){
		/* If time is more than 10ms */
		bounce_record = 0;
		Time_now_record = HAL_GetTick();

	} else {
		bounce_record = 0;

	}
	return bounce_record;
}
