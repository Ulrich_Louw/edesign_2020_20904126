/*
 * Global_variables.h
 *
 *  Created on: Feb 29, 2020
 *      Author: 20904126
 */

#ifndef INC_GLOBAL_VARIABLES_H_
#define INC_GLOBAL_VARIABLES_H_

#include <stdbool.h>

extern volatile uint8_t button;
extern bool recording;
extern uint32_t Time_now;
extern volatile bool REC;
extern volatile bool REC_1;
extern volatile bool playback;
extern volatile bool stop;
extern volatile bool first;
extern uint32_t last_tick;

#endif /* INC_GLOBAL_VARIABLES_H_ */
