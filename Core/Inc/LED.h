/*
 * LED.h
 *
 *  Created on: Feb 28, 2020
 *      Author: 20904126
 */

#ifndef INC_LED_H_
#define INC_LED_H_

uint8_t LED_function(uint8_t LED);

#endif /* INC_LED_H_ */
