/*
 * buttons.h
 *
 *  Created on: Feb 28, 2020
 *      Author: 20904126
 */

#ifndef INC_BUTTONS_H_
#define INC_BUTTONS_H_

#include <stdbool.h>

uint8_t Button_function(uint8_t button);
bool Check_Bounce(void);
void Stop_reset_function();
bool Check_StopBounce(void);
bool Check_RecordBounce(void);
#endif /* INC_BUTTONS_H_ */
